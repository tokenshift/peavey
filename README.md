# Peavey

Simple structured logging for Node.js.

## Overview

Structured logging allows you to treat your application logs like a queryable
time-ordered database. Instead of text munging using grep & regex, you can use
command-line tools like [jq](https://stedolan.github.io/jq/) or log aggregators
like Splunk or ElasticSearch to query your logs for particular fields, without
worrying that minor modifications to what you're logging will break all your
metrics & reports.

For some good information on what structured logging is and why you should use it,
see https://brandur.org/logfmt and https://engineering.grab.com/structured-logging.

This library follows patterns outlined by [logfmt](https://brandur.org/logfmt),
though it outputs in JSON format instead.

## Usage

```
const log = require('peavey');

log.level = log.LEVELS.WARN; // Don't emit DEBUG or INFO level logs
log.caller = true            // Add caller information to every log message

log.context.set('requestId', requestId);

log.info('Processing request', 'process/start', {
  requestId: requestId,
  requestPath: req.path
});
```

The following log levels are available:

* `ALL`
* `DEBUG`
* `INFO`
* `WARN`
* `ERROR`
* `FATAL`
* `OFF`

You can create additional loggers with their own context as well:

```
const log = require('peavey');

let logger = new log.Logger({
  caller: false,
  level: log.LEVELS.ALL
});
```