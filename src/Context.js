/**
 * Tracks context information/metadata that will be added to all log entries.
 * Useful for request IDs, things like that.
 */
class Context {
  constructor() {
    this.data = {};
  }
  /**
   * Add key/vals to the log context.
   * @param {string|Object} key - The key to add, or an object w/ multiple keys & values
   * @param {*=} val - The value to add (if the key was a string)
   */
  set(key, val) {
    if (typeof(key) === 'Object') {
      for (let entry of Object.entries(key)) {
        this.data[entry[0]] = entry[1];
      }
    }
    else if (typeof(key) === 'string') {
      this.data[key] = val;
    }
    else {
      throw TypeError(`Invalid key type: ${typeof(key)}`);
    }
  }

  /**
   * Remove a key from the context.
   */
  unset(key) {
    delete(this.data[key]);
  }

  /**
   * Remove all keys from the context.
   */
  clear() {
    this.data = {};
  }
}

module.exports = Context;