const Logger  = require('./Logger.js');

// Default logger
module.exports = new Logger();

// Hook to create a new logger
module.exports.Logger = Logger;