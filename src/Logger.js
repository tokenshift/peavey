const cycle = require('cycle');
const defaults = require('defaults');
const stackTrace = require('stack-trace');

const Context = require('./Context.js');

LOG_LEVELS = {
  'ALL':   0,
  'DEBUG': 1,
  'INFO':  2,
  'WARN':  3,
  'ERROR': 4,
  'FATAL': 5,
  'OFF':   6
}

for (let entry of Object.entries(LOG_LEVELS)) {
  LOG_LEVELS[entry[1]] = entry[0];
}

class Logger {
  constructor(options) {
    options = defaults(options, {
      caller: false,
      context: new Context(),
      level: LOG_LEVELS.ALL
    });

    this.caller = options.caller;
    this.context = options.context;
    this.level = options.level;

    this.LEVELS = LOG_LEVELS;
  }

  /** Gets the first function in the call stack that isn't part of this library. */
  _getCaller() {
    let trace = stackTrace.get();

    for (let caller of trace) {
      if (caller.getTypeName() != 'Logger') {
        return {
          typeName: caller.getTypeName(),
          functionName: caller.getFunctionName(),
          methodName: caller.getMethodName(),
          filePath: caller.getFileName(),
          lineNumber: caller.getLineNumber(),
          topLevelFlag: caller.isToplevel(),
          nativeFlag: caller.isNative(),
          evalFlag: caller.isEval(),
          evalOrigin: caller.getEvalOrigin()
        };
      }
    }

    return null;
  }

  /**
   * Checks whether a message should be logged given the current log level.
   * @param {number} level - The log level of the message to check.
   * @returns {boolean} Whether a message with the specified level should be logged.
   */
  _shouldLog(level) {
    return level >= this.level;
  }

  /**
   * Helper function to generate a log entry without writing it.
   * @param {number} level - The log level that this entry will be output at.
   * @param {string=} msg - Simple, human-readable log message.
   * @param {string=} tag - Unique identifier for this log entry.
   * @param {Object=} data - Additional data to include with the log entry.
   */
  _getEntry(level, msg, tag, data) {
    if (typeof(level) !== 'number') {
      throw new TypeError(`Invalid type for level: ${typeof(level)}`)
    }

    let meta = {
      '_ts': new Date().toISOString(),
      '_level': Logger.LOG_LEVELS[level]
    };

    if (typeof(msg) === 'string') {
      meta['_msg'] = msg;
      if (typeof(tag) === 'string') {
        meta['_tag'] = tag
      }
      else if (typeof(tag) === 'object') {
        data = tag;
      }
    }
    else if (typeof(msg) === 'object') {
      data = msg;
    }

    if (this.caller) {
      meta['_caller'] = this._getCaller();
    }

    return {
      ...meta,
      ...this.context.data,
      ...data
    };
  }

  /** Returns console.{log|warn|error} depending on the specified log level. */
  _outFn(level) {
    return [
      console.log,   // ALL
      console.debug, // DEBUG
      console.info,  // INFO
      console.warn,  // WARN
      console.error, // ERROR
      console.error  // FATAL
    ][level] || console.log;
  }

  /**
   * Output a single log entry.
   * @param {number} level - The log level that this entry will be output at.
   * @param {string=} msg - Simple, human-readable log message.
   * @param {string=} tag - Unique identifier for this log entry.
   * @param {Object=} data - Additional data to include with the log entry.
   * @returns The logged object (before serialization), or null if nothing was logged.
   */
  write(level, msg, tag, data) {
    if (!this._shouldLog(level)) {
      return null;
    }

    let entry = this._getEntry(level, msg, tag, data);
    let outFn = this._outFn(level);
    let json  = JSON.stringify(cycle.decycle(entry));
    outFn(json);

    return entry;
  }

  debug(...args) { return this.write(LOG_LEVELS.DEBUG, ...args); }
  info(...args)  { return this.write(LOG_LEVELS.INFO,  ...args); }
  warn(...args)  { return this.write(LOG_LEVELS.WARN,  ...args); }
  error(...args) { return this.write(LOG_LEVELS.ERROR, ...args); }
  fatal(...args) { return this.write(LOG_LEVELS.FATAL, ...args); }
}

Logger.LOG_LEVELS = LOG_LEVELS;
module.exports = Logger;