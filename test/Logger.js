const chai = require('chai');
chai.use(require('chai-string'));
const expect = chai.expect;

const Logger = require('../src/Logger.js');
let logger = new Logger();

describe('Logger', function () {
  describe('LOG_LEVELS', function () {
    it('converts names to numbers', function () {
      expect(logger.LEVELS['ALL']).to.eq(0);
      expect(logger.LEVELS['DEBUG']).to.eq(1);
      expect(logger.LEVELS['INFO']).to.eq(2);
      expect(logger.LEVELS['WARN']).to.eq(3);
      expect(logger.LEVELS['ERROR']).to.eq(4);
      expect(logger.LEVELS['FATAL']).to.eq(5);
      expect(logger.LEVELS['OFF']).to.eq(6);
    });

    it('converts numbers to names', function () {
      expect(logger.LEVELS[0]).to.eq('ALL');
      expect(logger.LEVELS[1]).to.eq('DEBUG');
      expect(logger.LEVELS[2]).to.eq('INFO');
      expect(logger.LEVELS[3]).to.eq('WARN');
      expect(logger.LEVELS[4]).to.eq('ERROR');
      expect(logger.LEVELS[5]).to.eq('FATAL');
      expect(logger.LEVELS[6]).to.eq('OFF');
    });
  });

  describe('_getCaller', function () {
    it('returns the calling function', function () {
      function testFun() {
        return logger._getCaller();
      }

      let caller = testFun();
      expect(caller.functionName).to.eq('testFun');
      expect(caller.filePath).to.endWith('peavey/test/Logger.js');
      expect(caller.lineNumber).to.eq(34);
    })
  });

  describe('_shouldLog', function () {
    it('compares the message level to the configured log level', function () {
      let logger = new Logger();
      logger.level = logger.LEVELS.WARN;
      expect(logger._shouldLog(logger.LEVELS.DEBUG)).to.be.false;
      expect(logger._shouldLog(logger.LEVELS.INFO)).to.be.false;
      expect(logger._shouldLog(logger.LEVELS.WARN)).to.be.true;
      expect(logger._shouldLog(logger.LEVELS.ERROR)).to.be.true;
      expect(logger._shouldLog(logger.LEVELS.FATAL)).to.be.true;
    });
  });

  describe('_getEntry', function () {
    it('includes the timestamp', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN);
      expect(entry).to.have.property('_ts');
      expect(entry._ts).to.be.a('string');
    });

    it('requires a log level', function () {
      expect(() => logger._getEntry()).to.throw();
    });

    it('includes the level as a string', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN);
      expect(entry._level).to.eq('WARN');
    });

    it('does not include the caller by default', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN);
      expect(entry).not.to.have.property('_caller');

      logger.caller = true;
      entry = logger._getEntry(logger.LEVELS.WARN);
      expect(entry).to.have.property('_caller');
    });

    it('can be called with a message', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN, 'test message');
      expect(entry).to.have.property('_msg');
      expect(entry._msg).to.eq('test message');
      expect(entry).not.to.have.property('_tag');
    });

    it('can be called with a message and tag', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN, 'test message', 'foo');
      expect(entry).to.have.property('_msg');
      expect(entry._msg).to.eq('test message');
      expect(entry).to.have.property('_tag');
      expect(entry._tag).to.eq('foo');
    });

    it('can be called with a message, tag and additional data', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN, 'test message', 'foo', {fizz: 'buzz'});
      expect(entry).to.have.property('_msg');
      expect(entry._msg).to.eq('test message');
      expect(entry).to.have.property('_tag');
      expect(entry._tag).to.eq('foo');
      expect(entry).to.have.property('fizz');
      expect(entry.fizz).to.eq('buzz');
    });

    it('can be called with a message and additional data', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN, 'test message', {fizz: 'buzz'});
      expect(entry).to.have.property('_msg');
      expect(entry._msg).to.eq('test message');
      expect(entry).not.to.have.property('_tag');
      expect(entry).to.have.property('fizz');
      expect(entry.fizz).to.eq('buzz');
    });

    it('can be called with additional data only', function () {
      let entry = logger._getEntry(logger.LEVELS.WARN, {fizz: 'buzz'});
      expect(entry).not.to.have.property('_msg');
      expect(entry).not.to.have.property('_tag');
      expect(entry).to.have.property('fizz');
      expect(entry.fizz).to.eq('buzz');
    });
  });

  describe('_outFn', function () {
    it('logs DEBUG to console.debug', function () {
      expect(logger._outFn(logger.LEVELS.DEBUG)).to.eq(console.debug);
    });

    it('logs INFO to console.info', function () {
      expect(logger._outFn(logger.LEVELS.INFO)).to.eq(console.info);
    });

    it('logs WARN to console.warn', function () {
      expect(logger._outFn(logger.LEVELS.WARN)).to.eq(console.warn);
    });

    it('logs ERROR to console.error', function () {
      expect(logger._outFn(logger.LEVELS.ERROR)).to.eq(console.error);
    });

    it('logs FATAL to console.error', function () {
      expect(logger._outFn(logger.LEVELS.FATAL)).to.eq(console.error);
    });
  });
});